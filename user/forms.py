from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from user.models import ExtendUser


class ExtendUserCreationForm(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['first_name', 'last_name','phone', 'email', 'username']

    def __init__(self, *args, **kwargs):
        super(ExtendUserCreationForm, self).__init__(*args, **kwargs)
