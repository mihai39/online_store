from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from django.shortcuts import render
from user.forms import ExtendUserCreationForm
from user.models import ExtendUser
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class UserCreateView(LoginRequiredMixin, PermissionRequiredMixin,CreateView):
    template_name = 'users/create_user.html'
    form_class = ExtendUserCreationForm
    success_url = '/user/create-user/'
    permission_required = 'product.view_product'


class UserDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'users/detele_user.html'
    model = User
    success_url = 'users/list-user/'
    #success_url = reversed_lazy('')
    context_object_name = 'user'
    permission_required = 'product.delete_product'


class UserListView(LoginRequiredMixin, PermissionRequiredMixin,ListView):
    template_name = 'users/list_user.html'
    model = User
    context_object_name = 'all_users'
    permission_required = 'product.view_product'


class UserUpdateView(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    template_name = 'users/update-user.html'
    model = User
    success_url = '/users/list-user/'
    form_class = UserCreationForm
    permission_required = 'product.change_product'

def visitor_counter(request):

    # Stocam numarul de vizitatori in variabila visit_count - variabila de sediuni
    visit_count = request.session.get('visits_count', 0)
    request.session['visits_count'] = visit_count + 1
    context = {
        'counter':visit_count
    }
    return render(request,'users/counter.html',context)