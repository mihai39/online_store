from django.urls import path

from user import views

urlpatterns = [
    path('create-user/', views.UserCreateView.as_view(), name='create_new_user'),
    path('delete-user/<int:pk>/', views.UserDeleteView.as_view(), name='delete_user'),
    path('list-user/', views.UserListView.as_view(), name='list_users'),
    path('update-user/<int:pk>/', views.UserUpdateView.as_view(), name='update_user'),
    path('counter/',views.visitor_counter, name = 'counter')
]