from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from rest_framework import viewsets
from category.forms import CategoryCreateForm
from category.models import Category
from category.serializers import CategorySerializer


class CategoryCreateView(CreateView):  # creare pe baza unui formular si adaugam in bd
    # pe baza unui model/formular, vom crea in bd o instanta/obiect campuri in bd
    template_name = 'category/category_create.html'

    # fields = [
    #     'category_name',
    #     'description'
    # ]
    # fields = '__all__'
    form_class = CategoryCreateForm
    success_url = reverse_lazy('category_list')
    permission_required = 'category_list'


class CategoryListView(LoginRequiredMixin, PermissionRequiredMixin,ListView):   # afisam toate afisarile dintr-un tabel in bd
    template_name = 'category/category_list.html'
    model = Category
    context_object_name = 'all_categories'
    permission_required = 'category_list'


class CategoryUpdateView(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    template_name = 'category/category_update.html'
    model = Category
    fields = '__all__'
    success_url = reverse_lazy('category_list')
    permission_required = 'category_update'


class CategoryDeleteView(LoginRequiredMixin, PermissionRequiredMixin,DeleteView):
    template_name = 'category/category_delete.html'
    model = Category
    success_url = reverse_lazy('category_list')
    permission_required = 'delete_category'


class CategoryDetailView(LoginRequiredMixin, PermissionRequiredMixin,DetailView):
    template_name = 'category/category_details.html'
    model = Category
    context_object_name = 'category_details'
    permission_required = 'category_details'


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

