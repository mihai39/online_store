import sqlite3
import xlrd

all_categories = xlrd.open_workbook('category.xls')
sheet = all_categories.sheet_by_name('Sheet1')

database = sqlite3.connect('../db.sqlite3')
cursor = database.cursor()

for r in range(1, sheet.nrows):
  id = int(sheet.cell(r, 0).value)
  name = sheet.cell(r, 1).value
  descriptions = sheet.cell(r,2).value

  #values = (name, )
  #query = "INSERT INTO categories_category(name) VALUES (?)"
  query = f"UPDATE category_category SET category_name='{name}', description = '{descriptions}' where id = {id}"
  cursor.execute(query)

cursor.close()
database.commit()
database.close()

columns = str(sheet.ncols)
rows = str(sheet.nrows)

print(f" I just insereted {columns} columns and {rows} to SQL")
