from django.urls import path, include

from category.views import CategoryListView, CategoryUpdateView, CategoryDeleteView, CategoryViewSet ,CategoryCreateView, \
    CategoryDetailView
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'category', CategoryViewSet)

urlpatterns = [
    path('category-create/', CategoryCreateView.as_view(), name='category_create'),
    path('category-list/', CategoryListView.as_view(), name='category_list'),
    path('category-update/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),
    path('category-delete/<int:pk>', CategoryDeleteView.as_view(), name='category_delete'),
    path('category-details/<int:pk>', CategoryDetailView.as_view(), name='category-details'),
    path('', include(router.urls)),
]
