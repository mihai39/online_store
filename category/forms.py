from django import forms
from django.forms import TextInput

from category.models import Category


class CategoryCreateForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'
        widgets = {
            'category_name': TextInput(attrs={
                'placeholder': 'Please insert category name.',
                'class': 'input is-primary'
            }),
            'description': TextInput(attrs={
                'placeholder': 'Please insert a description.',
                'class': 'input is-primary'
            })
        }

    def __init__(self, *args, **kwargs):
        super(CategoryCreateForm, self).__init__(*args, **kwargs)
        self.fields['category_name'].required = True
        self.fields['description'].required = True

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('category_name')
        description = cleaned_data.get('description')
        all_categories = Category.objects.all()
        for category in all_categories:
            if str(name) == str(category.category_name):
                msg = 'Category is already saved in Database.'
                self._errors['category_name'] = self.error_class([msg])
            if str(description) == str(category.description):
                msg2 = 'This description is already saved in Database.'
                self._errors['description'] = self.error_class([msg2])
        return cleaned_data
