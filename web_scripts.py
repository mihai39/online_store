import bs4
import requests
import lxml
import pandas as pd

url = 'https://www.worldometers.info/coronavirus/country/romania/'
page = requests.get(url)

soup = bs4.BeautifulSoup(page.text,"lxml")
total_case = soup.find_all("div",class_ = 'maincounter-number')

data = []

for index in total_case:
    cases = index.find("span")
    data.append(cases.string)

# print(data)

df =pd.DataFrame({'corona_data':data})
df.index = ['Total cases', 'Total deaths','Total recovered']
df.to_csv('corona_data.csv')
