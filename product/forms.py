from django import forms
from django.forms import TextInput, HiddenInput

from product.models import Product


class ProductCreateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'product_name': TextInput(attrs={
                'placeholder': 'Please insert category name.',
                'class': 'input is-primary'
            }),
            'description': TextInput(attrs={
                'placeholder': 'Please insert a description.',
                'class': 'input is-primary'
            }),
            'price': TextInput(attrs={
                'placeholder': 'Please insert a price.',
                'class': 'input is-primary'
            })
        }

    def __init__(self, *args, **kwargs):
        super(ProductCreateForm, self).__init__(*args, **kwargs)
        self.fields['product_name'].required = True
        self.fields['description'].required = True
        self.fields['price'].required = True



    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('product_name')
        description = cleaned_data.get('description')
        all_products = Product.objects.all()
        for product in all_products:
            if str(name) == str(product.product_name):
                msg = 'This product is already saved in Database.'
                self._errors['product_name'] = self.error_class([msg])
            if str(description) == str(product.description):
                msg2 = 'This description is already saved in Database.'
                self._errors['description'] = self.error_class([msg2])
        return cleaned_data
