from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=30)
    description = models.CharField(max_length=30)
    price = models.IntegerField()
    category = models.ForeignKey('category.Category', on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.product_name
