from django.urls import path, include

from product.views import ProductCreateView, ProductListView, ProductUpdateView, ProductDeleteView, ProductDetailView
from rest_framework import routers
from product.views import ProductViewSet

router = routers.DefaultRouter()
router.register(r'', ProductViewSet)

urlpatterns = [
    path('product-create/', ProductCreateView.as_view(), name='product-create'),
    path('product-list/', ProductListView.as_view(), name='list_of_products'),
    path('product-update/<int:pk>/', ProductUpdateView.as_view(), name='product-update'),
    path('product-delete/<int:pk>', ProductDeleteView.as_view(), name='product-delete'),
    path('product-details/<int:pk>', ProductDetailView.as_view(), name='product-details'),
    path('api/', include(router.urls)),
]
