from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from product.serializers import ProductSerializer
from product.forms import ProductCreateForm
from product.models import Product
from rest_framework import viewsets

class ProductCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'product/product_create.html'
    model = Product
    # fields = '__all__'
    # fields = [
    #     'product_name',
    #     'description',
    #     'price'
    # ]
    form_class = ProductCreateForm
    success_url = reverse_lazy('product-list')
    permission_required = 'product.change_product'


class ProductListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'product/product_list.html'
    model = Product
    context_object_name = 'all_products'
    permission_required = 'product.view_product'


class ProductUpdateView(LoginRequiredMixin, PermissionRequiredMixin,UpdateView):
    template_name = 'product/product_update.html'
    model = Product
    fields = '__all__'
    success_url = reverse_lazy('product-list')
    permission_required = 'product.update_product'

class ProductDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'product/product_delete.html'
    model = Product
    success_url = reverse_lazy('product-list')
    permission_required = 'product.delete_product'


class ProductDetailView(LoginRequiredMixin, PermissionRequiredMixin,DetailView):
    template_name = 'product/product_details.html'
    model = Product
    context_object_name = 'product_details'
    permission_required = 'product.details_product'


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
